﻿using Nop.Core;
using Nop.Core.Configuration;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.Ipag.Domain;
using Nop.Services.Configuration;
using Nop.Services.Payments;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Nop.Plugin.Payments.Ipag
{
    public class ApiHelper : ISettings
    {
        public class Token
        {
            public string token { get; set; }
            public string expires_at { get; set; }
        }

        public class Pagamento
        {
            public string id_transacao { get; set; }
            public string valor { get; set; }
            public string num_pedido { get; set; }
            public string status_pagamento { get; set; }
            public string metodo { get; set; }
            public string operadora { get; set; }
            public string operadora_mensagem { get; set; }
            public string id_librepag { get; set; }
            public string autorizacao_id { get; set; }
            public string url_autenticacao { get; set; }
            public string linha_digitavel { get; set; }
            public string token { get; set; }
            public string last4 { get; set; }
            public string mes { get; set; }
            public string ano { get; set; }
        }


        private readonly ISettingService _settingService;

        public HttpClient client = new HttpClient();
        public IpagPaymentSettings _ipagPaymentSettings = new IpagPaymentSettings();
        public readonly String ENDPOINT_SANDBOX = "https://sandbox.ipag.com.br/";
        public readonly String ENDPOINT_PRODUCTION = "https://www.librepag.com.br/";

        public async Task<string> GetAccessToken(IpagPaymentSettings ipagPaymentSettings )
        {
            _ipagPaymentSettings = ipagPaymentSettings;
            client.BaseAddress = new Uri(getEndpoint(ipagPaymentSettings));
            client.DefaultRequestHeaders.Accept.Clear();
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(_ipagPaymentSettings.LoginIpag + ":" + _ipagPaymentSettings.APIKey);
            var authToken = Convert.ToBase64String(plainTextBytes);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            Token token = null;
            HttpResponseMessage response = await client.GetAsync("service/sessionToken");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {

                token = await response.Content.ReadAsAsync<Token>();
            }
            return (string)token.token;

        }

        public string getEndpoint(IpagPaymentSettings ipagPaymentSettings)
        {
            if(ipagPaymentSettings.UseSandbox)
            {
                return ENDPOINT_SANDBOX;
            }
            return ENDPOINT_PRODUCTION;
        }
        public FormUrlEncodedContent CreatePayload(IpagPaymentSettings ipagPaymentSettings, IpagRequest ipagRequest)
        {

            string token="";
            string metodo = "visa";
            var custom = ipagRequest.request.CustomValues;
            token = (string)custom["plugins.payments.square.fields.tokenipag.key"];     
            metodo = (string)custom["plugins.payments.ipag.fields.methodipag.key"];
            var content = new FormUrlEncodedContent(new[]
            {
             new KeyValuePair<string, string>("identificacao", ipagPaymentSettings.LoginIpag),
            new KeyValuePair<string, string>("pedido", ipagRequest.order.id.ToString()),
            new KeyValuePair<string, string>("operacao", "Pagamento"),
            new KeyValuePair<string, string>("valor",ipagRequest.order.total.ToString()),
            new KeyValuePair<string, string>("metodo", metodo),
            new KeyValuePair<string, string>("url_retorno", "http://localhost:15536/payment/return"),
            new KeyValuePair<string, string>("email", ipagRequest.customer.Email),
            new KeyValuePair<string, string>("fone", ipagRequest.customer.BillingAddress.PhoneNumber),
            new KeyValuePair<string, string>("nome", ipagRequest.customer.BillingAddress.FirstName+" "+ipagRequest.customer.BillingAddress.LastName),
            new KeyValuePair<string, string>("endereco", ipagRequest.address.street),
            new KeyValuePair<string, string>("numero_endereco", ipagRequest.address.additional),
            new KeyValuePair<string, string>("bairro", ipagRequest.address.neighborhood),
            new KeyValuePair<string, string>("cidade", ipagRequest.address.city),
            new KeyValuePair<string, string>("estado", ipagRequest.address.state),
            new KeyValuePair<string, string>("cep", ipagRequest.address.cep.ToString()),
            new KeyValuePair<string, string>("retorno_tipo", "xml"),
            new KeyValuePair<string, string>("token_cartao", token),
            new KeyValuePair<string, string>("gera_token_cartao", "1"),
            });

            return content;


        }

        private PaymentStatus GetPaymentStatus(int Status)
        {
            switch (Status)
            {
                case 5:
                    return PaymentStatus.Authorized;

                case 8:
                    return PaymentStatus.Paid;

                case 7:
                    return PaymentStatus.Pending;

                case 3:
                    return PaymentStatus.Voided; //cancelado / estornado

                default:
                    return PaymentStatus.Pending;
            }
        }

        public ProcessPaymentResult SendPayload(IpagPaymentSettings ipagPaymentSettings, IpagRequest processPaymentRequest)
        {
            _ipagPaymentSettings = ipagPaymentSettings;
            client.BaseAddress = new Uri(getEndpoint(ipagPaymentSettings));
            client.DefaultRequestHeaders.Accept.Clear();
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(_ipagPaymentSettings.LoginIpag + ":" + _ipagPaymentSettings.APIKey);
            var authToken = Convert.ToBase64String(plainTextBytes);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("multipart/formdata"));


            var payload = CreatePayload(ipagPaymentSettings, processPaymentRequest);
            HttpResponseMessage response = client.PostAsync("service/payment",payload).Result;
            response.EnsureSuccessStatusCode();

            string resposta = response.Content.ReadAsStringAsync().Result;

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(resposta);

            var auth_transNode = xmlDoc.GetElementsByTagName("autorizacao_id");
            var auth_trans = auth_transNode[0].InnerXml;

            var statusNode = xmlDoc.GetElementsByTagName("status_pagamento");
            var status = statusNode[0].InnerXml;

            //var captureNode = xmlDoc.GetElementsByTagName("status_pagamento");
            //if(captureNode[0].)
            //var capture_id = captureNode[0].InnerXml;

            //Pagamento pag;
            //if (response.IsSuccessStatusCode)
            //{

            //     pag = response.Content.ReadAsAsync<Pagamento>().Result;
            //}


            ProcessPaymentResult pagamento = new ProcessPaymentResult {
                AuthorizationTransactionId = auth_trans,
                NewPaymentStatus = GetPaymentStatus(Convert.ToInt32(status)),
               
            };
         
          
           




            return pagamento;

        }

       
    }
}
