﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Payments.Ipag.Models;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.Ipag.Components
{
    [ViewComponent(Name = "PaymentIpag")]
    public class PaymentIpagViewComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var model = new PaymentInfoModel();
            return View("~/Plugins/Payments.Ipag/Views/PaymentInfo.cshtml",model);
        }
    }
}
