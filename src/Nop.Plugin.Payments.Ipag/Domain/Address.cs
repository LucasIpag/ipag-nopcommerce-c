﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Ipag.Domain
{
    public class Address
    {
        public string street;
        public string neighborhood;
        public string number;
        public string city;
        public string state;
        public int cep;
        public string additional;
        public string country;

        public Address(string street, string neighborhood,  string city, string state, int cep, string additional, string country)
        {
            this.street = street;
            this.neighborhood = neighborhood;
          
            this.city = city;
            this.state = state;
            this.cep = cep;
            this.additional = additional;
            this.country = country;
        }

        public Address()
        {
        }
    }
}
