﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Ipag.Domain;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Tax;
namespace Nop.Plugin.Payments.Ipag
{
    public class IpagPayment : BasePlugin, IPaymentMethod
    {
        #region Fields
        private readonly CurrencySettings _currencySettings;
        private readonly IpagPaymentSettings _ipagPaymentSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOrderService _orderService;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly ICustomerService _customerService;
        private readonly ICurrencyService _currencyService;
        private readonly ILogger _logger;

        public IpagPayment(CurrencySettings currencySettings, IpagPaymentSettings ipagPaymentSettings, IHttpContextAccessor httpContextAccessor, IOrderService orderService, IPaymentService paymentService, ISettingService settingService, ILocalizationService localizationService, IWebHelper webHelper, ICustomerService customerService, ICurrencyService currencyService, ILogger logger)
        {
            _currencySettings = currencySettings;
            _ipagPaymentSettings = ipagPaymentSettings;
            _httpContextAccessor = httpContextAccessor;
            _orderService = orderService;
            _paymentService = paymentService;
            _settingService = settingService;
            _localizationService = localizationService;
            _webHelper = webHelper;
            _customerService = customerService;
            _currencyService = currencyService;
            _logger = logger;
        }

        #endregion


        public bool SupportCapture { get { return false; } }

        public bool SupportPartiallyRefund { get { return false; } }

        public bool SupportRefund { get { return false; } }

        public bool SupportVoid { get { return false; } }

        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.NotSupported; }
        }

        public PaymentMethodType PaymentMethodType
        {
            get { return PaymentMethodType.Redirection; }
        }


        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        public override void Install()
        {
            //settings
            _settingService.SaveSetting(new IpagPaymentSettings
            {
                UseSandbox = true
            });

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Fields.BusinessEmail.Hint", "Specify your Ipag business email.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Fields.PassProductNamesAndTotals", "Pass product names and order totals to Ipag");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Fields.PassProductNamesAndTotals.Hint", "Check if product names and order totals should be passed to PayPal.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Fields.RedirectionTip", "Pagamento com Cartão de Crédito Ipag");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Fields.UseSandbox", "Use Sandbox");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.Instructions", "<p><b>If you're using this gateway ensure that your primary store currency is supported by Ipag.</b><br /><br />To use PDT, you must activate PDT and Auto Return in your Ipag account profile. You must also acquire a PDT identity token, which is used in all PDT communication you send to Ipag. Follow these steps to configure your account for PDT:<br /><br />1. Log in to your Ipag account (click <a href=\"https://painel.ipag.com.br\" target=\"_blank\">here</a> to create your account).<br />2. Click the Profile subtab.<br />3. Click Website Payment Preferences in the Seller Preferences column.<br />4. Under Auto Return for Website Payments, click the On radio button.<br />5. For the Return URL, enter the URL on your site that will receive the transaction ID posted by PayPal after a customer payment ({0}).<br />6. Under Payment Data Transfer, click the On radio button.<br />7. Click Save.<br />8. Click Website Payment Preferences in the Seller Preferences column.<br />9. Scroll down to the Payment Data Transfer section of the page to view your PDT identity token.<br /><br /></p>");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.PaymentMethodDescription", "Pagamento com Cartão de Crédito");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ipag.RoundingWarning", "It looks like you have \"ShoppingCartSettings.RoundPricesDuringCalculation\" setting disabled. Keep in mind that this can lead to a discrepancy of the order total amount, as Ipag only rounds to two decimals.");

            base.Install();
        }
        public string PaymentMethodDescription
        {
            get { return _localizationService.GetResource("Plugins.Payments.PayPalStandard.PaymentMethodDescription"); }
        }

        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            return null;
        }

        public bool CanRePostProcessPayment(Order order)
        {
            return false;
        }

        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            return null;
        }

        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            decimal taxa = 0;
            return taxa;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentIpag/Configure";
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            var paymentRequest = new ProcessPaymentRequest();

            //pass custom values to payment processor
            if (form.TryGetValue("ipag-token", out StringValues ipagToken) && !StringValues.IsNullOrEmpty(ipagToken))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Ipag.Fields.IpagToken.Key"), ipagToken.ToString());

            if (form.TryGetValue("TokenIpag", out StringValues TokenIpag) && !StringValues.IsNullOrEmpty(TokenIpag) && !TokenIpag.Equals("0"))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Square.Fields.TokenIpag.Key"), TokenIpag.ToString());

            if (form.TryGetValue("SaveCard", out StringValues saveCardValue) && !StringValues.IsNullOrEmpty(saveCardValue) && bool.TryParse(saveCardValue[0], out bool saveCard) && saveCard)
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Square.Fields.SaveCard.Key"), saveCard);

            if (form.TryGetValue("MethodIpag", out StringValues method) && !StringValues.IsNullOrEmpty(method) && !method.Equals("0"))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Ipag.Fields.MethodIpag.Key"), method.ToString());


            return paymentRequest;
        }

        public string GetPublicViewComponentName()
        {
            return  "PaymentIpag";
        }

        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            return false;
        }

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
   
        }

        private IpagRequest CreateRequest(ProcessPaymentRequest processPaymentRequest)
        {
            //get customer
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            if (customer == null)
                throw new NopException("Customer cannot be loaded");
            #region Currency
            ////get the primary store currency
            //var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            //if (currency == null)
            //    throw new NopException("Primary store currency cannot be loaded");

            //whether the currency is supported by the Square
            // if (!Enum.TryParse(currency.CurrencyCode, out SquareModel.Money.CurrencyEnum moneyCurrency))
            //  throw new NopException($"The {currency.CurrencyCode} currency is not supported by the Square");
            #endregion
            //check customer's billing address, shipping address and email, 
            Domain.Address address = new Domain.Address(
                customer.BillingAddress.Address1,
                customer.BillingAddress.County,
                customer.BillingAddress.City,
                customer.BillingAddress.StateProvince.Name,
                Convert.ToInt32(customer.BillingAddress.ZipPostalCode),
                customer.BillingAddress.Address2,
                customer.BillingAddress.Country.Name
                );
            IpagOrder ordem = new IpagOrder(processPaymentRequest.OrderGuid.ToString(), processPaymentRequest.OrderTotal);
            IpagRequest request = new IpagRequest(
                customer,
                address,
                ordem,
                processPaymentRequest
                );
            #region Default Request
            //create common charge request parameters
            //var chargeRequest = new ExtendedChargeRequest
            //(
            //    AmountMoney: amountMoney,
            //    BillingAddress: billingAddress,
            //    BuyerEmailAddress: email,
            //    DelayCapture: _squarePaymentSettings.TransactionMode == TransactionMode.Authorize,
            //    IdempotencyKey: Guid.NewGuid().ToString(),
            //    IntegrationId: !string.IsNullOrEmpty(SquarePaymentDefaults.IntegrationId) ? SquarePaymentDefaults.IntegrationId : null,
            //    Note: string.Format(SquarePaymentDefaults.PaymentNote, paymentRequest.OrderGuid),
            //    ReferenceId: paymentRequest.OrderGuid.ToString(),
            //    ShippingAddress: shippingAddress
            //);

            #endregion
            //try to get previously stored card details
            #region Checks
            //var storedCardKey = _localizationService.GetResource("Plugins.Payments.Square.Fields.StoredCard.Key");
            //if (paymentRequest.CustomValues.TryGetValue(storedCardKey, out object storedCardId) && !storedCardId.ToString().Equals("0"))
            //{
            //    //check whether customer exists
            //    var customerId = _genericAttributeService.GetAttribute<string>(customer, SquarePaymentDefaults.CustomerIdAttribute);
            //    var squareCustomer = _squarePaymentManager.GetCustomer(customerId);
            //    if (squareCustomer == null)
            //        throw new NopException("Failed to retrieve customer");

            //    //set 'card on file' to charge
            //    chargeRequest.CustomerId = squareCustomer.Id;
            //    chargeRequest.CustomerCardId = storedCardId.ToString();
            //    return chargeRequest;
            //}
            #endregion
            #region GetCardNounce
            //or try to get the card nonce
            //var cardNonceKey = _localizationService.GetResource("Plugins.Payments.Square.Fields.CardNonce.Key");
            //if (!paymentRequest.CustomValues.TryGetValue(cardNonceKey, out object cardNonce) || string.IsNullOrEmpty(cardNonce?.ToString()))
            //    throw new NopException("Failed to get the card nonce");

            ////remove the card nonce from payment custom values, since it is no longer needed
            //paymentRequest.CustomValues.Remove(cardNonceKey);

            ////whether to save card details for the future purchasing
            //var saveCardKey = _localizationService.GetResource("Plugins.Payments.Square.Fields.SaveCard.Key");
            //if (paymentRequest.CustomValues.TryGetValue(saveCardKey, out object saveCardValue) && saveCardValue is bool saveCard && saveCard && !customer.IsGuest())
            //{
            //    //remove the value from payment custom values, since it is no longer needed
            //    paymentRequest.CustomValues.Remove(saveCardKey);

            //    try
            //    {
            //        //check whether customer exists
            //        var customerId = _genericAttributeService.GetAttribute<string>(customer, SquarePaymentDefaults.CustomerIdAttribute);
            //        var squareCustomer = _squarePaymentManager.GetCustomer(customerId);

            //        if (squareCustomer == null)
            //        {
            //            //try to create the new one, if not exists
            //            var customerRequest = new SquareModel.CreateCustomerRequest
            //            (
            //                EmailAddress: customer.Email,
            //                Nickname: customer.Username,
            //                GivenName: _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute),
            //                FamilyName: _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute),
            //                PhoneNumber: _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute),
            //                CompanyName: _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute),
            //                ReferenceId: customer.CustomerGuid.ToString()
            //            );
            //            squareCustomer = _squarePaymentManager.CreateCustomer(customerRequest);
            //            if (squareCustomer == null)
            //                throw new NopException("Failed to create customer. Error details in the log");

            //            //save customer identifier as generic attribute
            //            _genericAttributeService.SaveAttribute(customer, SquarePaymentDefaults.CustomerIdAttribute, squareCustomer.Id);
            //        }

            //        //create request parameters to create the new card
            //        var cardRequest = new SquareModel.CreateCustomerCardRequest
            //        (
            //            BillingAddress: chargeRequest.BillingAddress ?? chargeRequest.ShippingAddress,
            //            CardNonce: cardNonce.ToString()
            //        );

            //        //set postal code
            //        var postalCodeKey = _localizationService.GetResource("Plugins.Payments.Square.Fields.PostalCode.Key");
            //        if (paymentRequest.CustomValues.TryGetValue(postalCodeKey, out object postalCode) && !string.IsNullOrEmpty(postalCode.ToString()))
            //        {
            //            //remove the value from payment custom values, since it is no longer needed
            //            paymentRequest.CustomValues.Remove(postalCodeKey);

            //            cardRequest.BillingAddress = cardRequest.BillingAddress ?? new SquareModel.Address();
            //            cardRequest.BillingAddress.PostalCode = postalCode.ToString();
            //        }

            //        //try to create card
            //        var card = _squarePaymentManager.CreateCustomerCard(squareCustomer.Id, cardRequest);
            //        if (card == null)
            //            throw new NopException("Failed to create card. Error details in the log");

            //        //save card identifier to payment custom values for further purchasing
            //        if (isRecurringPayment)
            //            paymentRequest.CustomValues.Add(storedCardKey, card.Id);

            //        //set 'card on file' to charge
            //        chargeRequest.CustomerId = squareCustomer.Id;
            //        chargeRequest.CustomerCardId = card.Id;
            //        return chargeRequest;
            //    }
            //    catch (Exception exception)
            //    {
            //        _logger.Warning(exception.Message, exception, customer);
            //        if (isRecurringPayment)
            //            throw new NopException("For recurring payments you need to save the card details");
            //    }
            //}
            //else if (isRecurringPayment)
            //    throw new NopException("For recurring payments you need to save the card details");

            ////set 'card nonce' to charge
            //chargeRequest.CardNonce = cardNonce.ToString();
            #endregion
            return request;
        }

        private ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest, bool isRecurringPayment)
        {
            #region previousRequests
            ////create request parameters
            //var request = CreateChargeRequest(paymentRequest, isRecurringPayment);

            ////charge transaction
            //var transaction = _worldpayPaymentSettings.TransactionMode == TransactionMode.Authorize
            //    ? _worldpayPaymentManager.Authorize(new AuthorizeRequest(request)) : _worldpayPaymentManager.Charge(request)
            //    ?? throw new NopException("An error occurred while processing. Error details in the log");

            ////save card identifier to payment custom values for further purchasing
            //if (isRecurringPayment && !string.IsNullOrEmpty(transaction.VaultData?.Token?.PaymentMethodId))
            //    paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Worldpay.Fields.StoredCard.Key"), transaction.VaultData.Token.PaymentMethodId);

            ////return result
            //var result = new ProcessPaymentResult
            //{
            //    AvsResult = $"{transaction.AvsResult}. Code: {transaction.AvsCode}",
            //    Cvv2Result = $"{transaction.CvvResult}. Code: {transaction.CvvCode}",
            //    AuthorizationTransactionCode = transaction.AuthorizationCode
            //};

            //if (_worldpayPaymentSettings.TransactionMode == TransactionMode.Authorize)
            //{
            //    result.AuthorizationTransactionId = transaction.TransactionId.ToString();
            //    result.AuthorizationTransactionResult = transaction.ResponseText;
            //    result.NewPaymentStatus = PaymentStatus.Authorized;
            //}

            //if (_worldpayPaymentSettings.TransactionMode == TransactionMode.Charge)
            //{
            //    result.CaptureTransactionId = transaction.TransactionId.ToString();
            //    result.CaptureTransactionResult = transaction.ResponseText;
            //    result.NewPaymentStatus = PaymentStatus.Paid;
            //}

            //return result;
            #endregion

            IpagRequest paymentRequest = CreateRequest(processPaymentRequest);
           ApiHelper api = new ApiHelper();
           ProcessPaymentResult result =  api.SendPayload(_ipagPaymentSettings, paymentRequest);
           return result;

        }


       

        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            return null;
        }

        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            return null;
        }

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            //try to get errors
            if (form.TryGetValue("Errors", out StringValues errorsString) && !StringValues.IsNullOrEmpty(errorsString))
                return errorsString.ToString().Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return new List<string>();
        }

        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            return null;
        }

        public ProcessPaymentResult ProcessPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            return ProcessPayment(processPaymentRequest,false);
        }
    }
}
