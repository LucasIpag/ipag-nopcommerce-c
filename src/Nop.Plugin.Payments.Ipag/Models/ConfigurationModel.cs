﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Ipag.Models
{
    [Validator(typeof(ConfigurationModelValidator))]
    public class ConfigurationModel :BaseNopModel
    {
        public ConfigurationModel()
        {
            Locations = new List<SelectListItem>();
            Cards = new List<string>();
            Cards.Add("Visa");
            Cards.Add("MasterCard");
            Cards.Add("Diners");
            Cards.Add("Discover");
            Cards.Add("Elo");
            Cards.Add("American Express");
            Cards.Add("Hipercard");
            Cards.Add("JCB");
            Cards.Add("Aura");
        }


        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("API Key")]
        public string APIKey { get; set; }
        public bool APIKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Login Ipag")]
        public string LoginIpag { get; set; }
        public bool LoginIpag_OverrideForStore { get; set; }

       
        [NopResourceDisplayName("Usar Sandbox")]
        public bool UseSandbox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Cartões Aceitos")]
        public IList<string> AcceptedCards { get; set; }
        public IList<string> Cards { get; set; }
        public string[] CheckedAcceptedCards { get; set; }
        public bool AcceptedCards_OverrideForStore { get; set; }

        [NopResourceDisplayName("Location")]
        public string LocationId { get; set; }
        public IList<SelectListItem> Locations { get; set; }
    }
}
