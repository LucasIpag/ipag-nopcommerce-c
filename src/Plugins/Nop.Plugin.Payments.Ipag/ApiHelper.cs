﻿using Nop.Core;
using Nop.Core.Configuration;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Plugin.Payments.Ipag.Domain;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Nop.Plugin.Payments.Ipag
{
    public class ApiHelper : ISettings
    {
        public class Token
        {
            public string token { get; set; }
            public string expires_at { get; set; }
        }

        public class Pagamento
        {
            public string id_transacao { get; set; }
            public string valor { get; set; }
            public string num_pedido { get; set; }
            public string status_pagamento { get; set; }
            public string metodo { get; set; }
            public string operadora { get; set; }
            public string operadora_mensagem { get; set; }
            public string id_librepag { get; set; }
            public string autorizacao_id { get; set; }
            public string url_autenticacao { get; set; }
            public string linha_digitavel { get; set; }
            public string token { get; set; }
            public string last4 { get; set; }
            public string mes { get; set; }
            public string ano { get; set; }
        }


        private readonly ISettingService _settingService;
        public ICustomNumberFormatter _customNumberFormatter;
        public HttpClient client = new HttpClient();
        public IpagPaymentSettings _ipagPaymentSettings = new IpagPaymentSettings();
        public readonly String ENDPOINT_SANDBOX = "https://sandbox.ipag.com.br/";
        public readonly String ENDPOINT_PRODUCTION = "https://www.librepag.com.br/";
        private readonly IWebHelper _webHelper;
        private readonly IEncryptionService _encryptionService;
        private readonly IPaymentService _paymentService;
       


        public  string GetAccessToken(IpagPaymentSettings ipagPaymentSettings, bool isClicked )
        {
            
                _ipagPaymentSettings = ipagPaymentSettings;
                client.BaseAddress = new Uri(getEndpoint(ipagPaymentSettings));
                client.DefaultRequestHeaders.Accept.Clear();
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(_ipagPaymentSettings.LoginIpag + ":" + _ipagPaymentSettings.APIKey);
                var authToken = Convert.ToBase64String(plainTextBytes);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "iPag plugin NopCommerce");
                Token token = null;
                HttpResponseMessage response = client.GetAsync("service/sessionToken").Result;
                if (response.IsSuccessStatusCode)
                {
                    token = response.Content.ReadAsAsync<Token>().Result;
                    return token.token;
                }
                else
                {
                    return "Erro ao obter Token de Acesso";
                }
            
          
        }

        public string getEndpoint(IpagPaymentSettings ipagPaymentSettings)
        {
            if(ipagPaymentSettings.UseSandbox)
            {
                return ENDPOINT_SANDBOX;
            }
            return ENDPOINT_PRODUCTION;
        }

        public Order criarOrder(IpagRequest request, IWebHelper webHelper)
        {
            
            TaxDisplayType tx = new TaxDisplayType();
            Decimal rate = 0;
            ProcessPaymentResult pr = new ProcessPaymentResult();
            ShippingStatus status = new ShippingStatus();
            try
            {
                var order = new Order
                {
                    StoreId = request.request.StoreId,
                    OrderGuid = request.request.OrderGuid,
                    CustomerId = request.request.CustomerId,
                    CustomerLanguageId = 1,
                    CustomerTaxDisplayType = TaxDisplayType.ExcludingTax,
                    CustomerIp =webHelper.GetCurrentIpAddress(),
                    OrderSubtotalInclTax = rate,
                    OrderSubtotalExclTax = rate,
                    OrderSubTotalDiscountInclTax = rate,
                    OrderSubTotalDiscountExclTax = rate,
                    OrderShippingInclTax = rate,
                    OrderShippingExclTax = rate,
                    PaymentMethodAdditionalFeeInclTax = rate,
                    PaymentMethodAdditionalFeeExclTax = rate,
                    TaxRates = "",
                    OrderTax = rate,
                    OrderTotal = rate,
                    RefundedAmount = decimal.Zero,
                    OrderDiscount = rate,
                    CheckoutAttributeDescription = "",
                    CheckoutAttributesXml = "",
                    CustomerCurrencyCode = "",
                    CurrencyRate = rate,
                    AffiliateId = 0,
                    OrderStatus = OrderStatus.Pending,
                    AllowStoringCreditCardNumber =false,
                    CardType = "",
                    CardName ="",
                    CardNumber = "",
                    MaskedCreditCardNumber = "",
                    CardCvv2 = "" ,
                    CardExpirationMonth ="" ,
                    CardExpirationYear = "" ,
                    PaymentMethodSystemName = request.request.PaymentMethodSystemName,
                    AuthorizationTransactionId = "123123123",
                    AuthorizationTransactionCode = "23234",
                    AuthorizationTransactionResult = "",
                    CaptureTransactionId = "12",
                    CaptureTransactionResult = "",
                    SubscriptionTransactionId = "",
                    PaymentStatus = Nop.Core.Domain.Payments.PaymentStatus.Authorized,
                    PaidDateUtc = DateTime.UtcNow,
                    BillingAddress = request.customer.BillingAddress,
                    ShippingAddress = request.customer.ShippingAddress,
                    ShippingStatus = ShippingStatus.NotYetShipped,
                    ShippingMethod = "",
                    PickUpInStore = true,
                    PickupAddress = request.customer.ShippingAddress,
                    ShippingRateComputationMethodSystemName = "",
                    CustomValuesXml = "",
                    VatNumber = "",
                    CreatedOnUtc = DateTime.UtcNow,
                    CustomOrderNumber = string.Empty
                };
                return order;
            }
            catch (Exception exc)
            {
               
            }
            return null;
           
        }


        public FormUrlEncodedContent CreatePayload(IpagPaymentSettings ipagPaymentSettings, IpagRequest ipagRequest, IWebHelper web, ICustomNumberFormatter customNumberFormatter)
        {

            string token="";
            string metodo = "visa";
            // Order ord = criarOrder(ipagRequest, web);
            DateTime date = DateTime.UtcNow;
            var order_number = date.Year + "" + date.Month + "" + date.Day + "" + date.Hour+""+date.Minute+"" + date.Second;
         
            // var order_number = customNumberFormatter.GenerateOrderCustomNumber(ord);

            var custom = ipagRequest.request.CustomValues;
            token = (string)custom["plugins.payments.ipag.fields.ipagtoken.key"];     
            metodo = (string)custom["plugins.payments.ipag.fields.methodipag.key"];
            custom.Remove("plugins.payments.ipag.fields.ipagtoken.key");
            custom.Remove("plugins.payments.ipag.fields.methodipag.key");


            var content = new FormUrlEncodedContent(new[]
            {
             new KeyValuePair<string, string>("identificacao", ipagPaymentSettings.LoginIpag),
            new KeyValuePair<string, string>("pedido", order_number),
            new KeyValuePair<string, string>("operacao", "Pagamento"),
            new KeyValuePair<string, string>("valor",ipagRequest.order.total.ToString()),
            new KeyValuePair<string, string>("metodo", metodo),
            new KeyValuePair<string, string>("url_retorno", "http://localhost:15536/payment/return"),
            new KeyValuePair<string, string>("email", ipagRequest.customer.Email),
            new KeyValuePair<string, string>("fone", ipagRequest.customer.BillingAddress.PhoneNumber),
            new KeyValuePair<string, string>("nome", ipagRequest.customer.BillingAddress.FirstName+" "+ipagRequest.customer.BillingAddress.LastName),
            new KeyValuePair<string, string>("endereco", ipagRequest.address.street),
            new KeyValuePair<string, string>("numero_endereco", ipagRequest.address.additional),
            new KeyValuePair<string, string>("bairro", ipagRequest.address.neighborhood),
            new KeyValuePair<string, string>("cidade", ipagRequest.address.city),
            new KeyValuePair<string, string>("estado", ipagRequest.address.state),
            new KeyValuePair<string, string>("cep", ipagRequest.address.cep.ToString()),
            new KeyValuePair<string, string>("retorno_tipo", "xml"),
            new KeyValuePair<string, string>("token_cartao", token),
            new KeyValuePair<string, string>("gera_token_cartao", "1"),
            });
           
            return content;


        }

        private PaymentStatus GetPaymentStatus(int Status)
        {
            switch (Status)
            {
                case 5:
                    return PaymentStatus.Authorized;

                case 8:
                    return PaymentStatus.Paid;

                case 7:
                    return PaymentStatus.Pending;

                case 3:
                    return PaymentStatus.Voided; //cancelado / estornado

                default:
                    return PaymentStatus.Pending;
            }
        }

        public ProcessPaymentResult SendPayload(IpagPaymentSettings ipagPaymentSettings, IpagRequest processPaymentRequest, IWebHelper web, ICustomNumberFormatter customNumberFormatter, ILocalizationService _localizationService)
        {
            _ipagPaymentSettings = ipagPaymentSettings;
            client.BaseAddress = new Uri(getEndpoint(ipagPaymentSettings));
            client.DefaultRequestHeaders.Accept.Clear();
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(_ipagPaymentSettings.LoginIpag + ":" + _ipagPaymentSettings.APIKey);
            var authToken = Convert.ToBase64String(plainTextBytes);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("multipart/formdata"));
            client.DefaultRequestHeaders.Add("User-Agent", "iPag plugin NopCommerce");



            var payload = CreatePayload(ipagPaymentSettings, processPaymentRequest, web, customNumberFormatter);
            HttpResponseMessage response = client.PostAsync("service/payment",payload).Result;

            string resposta = response.Content.ReadAsStringAsync().Result;


            var xmlDoc = new XmlDocument();
            
            xmlDoc.LoadXml(resposta);


            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Problemas ao completar transação de pagamento. Tente novamente mais tarde ! ");

            }
            if (xmlDoc.GetElementsByTagName("error").Count>0)
            {
                throw new Exception("Problemas ao completar transação de pagamento: "+ xmlDoc.GetElementsByTagName("message")[0].InnerXml);
            }
            var auth_transNode = xmlDoc.GetElementsByTagName("autorizacao_id");
            var auth_trans = auth_transNode[0].InnerXml;

            var statusNode = xmlDoc.GetElementsByTagName("status_pagamento");
            var status = statusNode[0].InnerXml;

            var numpedidoNode = xmlDoc.GetElementsByTagName("num_pedido");
            var numpedido = numpedidoNode[0].InnerXml;



            processPaymentRequest.request.CustomValues.Add(_localizationService.GetResource("IPAG ID"), numpedido);





            ProcessPaymentResult pagamento = new ProcessPaymentResult {
                AuthorizationTransactionId = auth_trans,
                NewPaymentStatus = GetPaymentStatus(Convert.ToInt32(status)),
               
            };
         
          
           




            return pagamento;

        }

       
    }
}
