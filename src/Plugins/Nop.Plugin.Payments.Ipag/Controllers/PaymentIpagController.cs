﻿using Nop.Core;
using Nop.Plugin.Payments.Ipag.Models;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;

namespace Nop.Plugin.Payments.Ipag.Controllers
{
   public class PaymentIpagController : BasePaymentController
    {
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        public readonly String ENDPOINT_SANDBOX = "https://sandbox.ipag.com.br/";
        public readonly String ENDPOINT_PRODUCTION = "https://www.librepag.com.br/";
        private IpagPaymentSettings _ipagPaymentSettings;
        public class Token
        {
            public string token { get; set; }
            public string expires_at { get; set; }
        }
        public PaymentIpagController(IPermissionService permissionService, ISettingService settingService, IStoreContext storeContext, ILocalizationService localizationService, IWorkContext workContext, IpagPaymentSettings ipagPaymentSettings)
        {
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
            _localizationService = localizationService;
            _workContext = workContext;
            _ipagPaymentSettings = ipagPaymentSettings;
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
            //    return AccessDeniedView();

            ////load settings for a chosen store scope
            //var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            //ISettingService _settingService;
            //var IpagPaymentSettings = _settingService.LoadSetting<IpagPaymentSettings>();

            var model = new ConfigurationModel
            {
                UseSandbox = _ipagPaymentSettings.UseSandbox,
                AcceptedCards = _ipagPaymentSettings.AcceptedCards,
                APIKey = _ipagPaymentSettings.APIKey,
                LoginIpag = _ipagPaymentSettings.LoginIpag,
             
            };
            //if (storeScope > 0)
            //{
            //    model.UseSandbox_OverrideForStore = _settingService.SettingExists(IpagPaymentSettings, x => x.UseSandbox, storeScope);
            //    model.APIKey_OverrideForStore = _settingService.SettingExists(IpagPaymentSettings, x => x.APIKey, storeScope);
            //    model.LoginIpag_OverrideForStore = _settingService.SettingExists(IpagPaymentSettings, x => x.LoginIpag, storeScope);
            //    model.AcceptedCards_OverrideForStore = _settingService.SettingExists(IpagPaymentSettings, x => x.AcceptedCards, storeScope);

            //}

            return View("~/Plugins/Payments.Ipag/Views/Configure.cshtml", model);
        }


        [HttpPost, ActionName("Configure")]
        [FormValueRequired("save")]
        [AdminAuthorize]
        [ChildActionOnly]
        [ValidateAntiForgeryToken]
        public ActionResult Configure(ConfigurationModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
            //    return AccessDeniedView();
            var m = model.AcceptedCards;
            if (!ModelState.IsValid)
                return Configure();
            List<string> AcceptedCards = new List<string>() ;

            //var values = model.Form["AcceptedCards"];
          
            //var splited = values.ToArray();
            //for(int i=0;i<=splited.Length-1;i++)
            //{
            //    AcceptedCards.Add(splited[i]);
            //}
            //load settings for a chosen store scope
            //var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            //var IpagPaymentSettings = _settingService.LoadSetting<IpagPaymentSettings>(storeScope);
            model.AcceptedCards = AcceptedCards;
            //save settings
            _ipagPaymentSettings.UseSandbox = model.UseSandbox;
            _ipagPaymentSettings.LoginIpag = model.LoginIpag;
            _ipagPaymentSettings.APIKey = model.APIKey;
            _ipagPaymentSettings.AcceptedCards = AcceptedCards;
            _settingService.SaveSetting(_ipagPaymentSettings);
            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            //_settingService.SaveSettingOverridablePerStore(IpagPaymentSettings, x => x.UseSandbox, model.UseSandbox_OverrideForStore, storeScope, false);
            //_settingService.SaveSettingOverridablePerStore(IpagPaymentSettings, x => x.LoginIpag, model.LoginIpag_OverrideForStore, storeScope, false);
            //_settingService.SaveSettingOverridablePerStore(IpagPaymentSettings, x => x.APIKey, model.APIKey_OverrideForStore, storeScope, false);
            //_settingService.SaveSettingOverridablePerStore(IpagPaymentSettings, x => x.AcceptedCards, model.AcceptedCards_OverrideForStore, storeScope, false);
            ////now clear settings cache
            //_settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }
        
        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            var model = new PaymentInfoModel
            {

                //whether current customer is guest
                IsGuest = _workContext.CurrentCustomer.IsGuest(),

                ////get postal code from the billing address or from the shipping one
                //PostalCode = _workContext.CurrentCustomer.BillingAddress?.ZipPostalCode
                //?? _workContext.CurrentCustomer.ShippingAddress?.ZipPostalCode
            };

            //whether customer already has stored cards
            //var customerId = _workContext.CurrentCustomer.GetAttribute<string>(SquarePaymentDefaults.CustomerIdAttribute);
            //var customer = _squarePaymentManager.GetCustomer(customerId);
            //if (customer?.Cards != null)
            //{
            //    var cardNumberMask = _localizationService.GetResource("Plugins.Payments.Square.Fields.StoredCard.Mask");
            //    model.StoredCards = customer.Cards.Select(card => new SelectListItem { Text = string.Format(cardNumberMask, card.Last4), Value = card.Id }).ToList();
            //}

            //add the special item for 'select card' with value 0
            //if (model.StoredCards.Any())
            //{
            //    var selectCardText = _localizationService.GetResource("Plugins.Payments.Square.Fields.StoredCard.SelectCard");
            //    model.StoredCards.Insert(0, new SelectListItem { Text = selectCardText, Value = "0" });
            //}

            return View("~/Plugins/Payments.Ipag/Views/PaymentInfo.cshtml", model);
        }
        public ActionResult IPNHandler()
        {
            /*byte[] parameters;
            using (var stream = new MemoryStream())
            {
                this.Request.Body.CopyTo(stream);
                parameters = stream.ToArray();
            }
            var strRequest = Encoding.ASCII.GetString(parameters);

            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.PayPalStandard") as PayPalStandardPaymentProcessor;
            if (processor == null ||
                !_paymentService.IsPaymentMethodActive(processor) || !processor.PluginDescriptor.Installed)
                throw new NopException("PayPal Standard module cannot be loaded");

            if (processor.VerifyIpn(strRequest, out Dictionary<string, string> values))
            {
                #region values
                var mc_gross = decimal.Zero;
                try
                {
                    mc_gross = decimal.Parse(values["mc_gross"], new CultureInfo("en-US"));
                }
                catch { }

                values.TryGetValue("payer_status", out string payer_status);
                values.TryGetValue("payment_status", out string payment_status);
                values.TryGetValue("pending_reason", out string pending_reason);
                values.TryGetValue("mc_currency", out string mc_currency);
                values.TryGetValue("txn_id", out string txn_id);
                values.TryGetValue("txn_type", out string txn_type);
                values.TryGetValue("rp_invoice_id", out string rp_invoice_id);
                values.TryGetValue("payment_type", out string payment_type);
                values.TryGetValue("payer_id", out string payer_id);
                values.TryGetValue("receiver_id", out string receiver_id);
                values.TryGetValue("invoice", out string _);
                values.TryGetValue("payment_fee", out string payment_fee);

                #endregion

                var sb = new StringBuilder();
                sb.AppendLine("PayPal IPN:");
                foreach (var kvp in values)
                {
                    sb.AppendLine(kvp.Key + ": " + kvp.Value);
                }

                var newPaymentStatus = PayPalHelper.GetPaymentStatus(payment_status, pending_reason);
                sb.AppendLine("New payment status: " + newPaymentStatus);

                switch (txn_type)
                {
                    case "recurring_payment_profile_created":
                        //do nothing here
                        break;
                    #region Recurring payment
                    case "recurring_payment":
                        {
                            var orderNumberGuid = Guid.Empty;
                            try
                            {
                                orderNumberGuid = new Guid(rp_invoice_id);
                            }
                            catch
                            {
                            }

                            var initialOrder = _orderService.GetOrderByGuid(orderNumberGuid);
                            if (initialOrder != null)
                            {
                                var recurringPayments = _orderService.SearchRecurringPayments(initialOrderId: initialOrder.Id);
                                foreach (var rp in recurringPayments)
                                {
                                    switch (newPaymentStatus)
                                    {
                                        case PaymentStatus.Authorized:
                                        case PaymentStatus.Paid:
                                            {
                                                var recurringPaymentHistory = rp.RecurringPaymentHistory;
                                                if (!recurringPaymentHistory.Any())
                                                {
                                                    //first payment
                                                    var rph = new RecurringPaymentHistory
                                                    {
                                                        RecurringPaymentId = rp.Id,
                                                        OrderId = initialOrder.Id,
                                                        CreatedOnUtc = DateTime.UtcNow
                                                    };
                                                    rp.RecurringPaymentHistory.Add(rph);
                                                    _orderService.UpdateRecurringPayment(rp);
                                                }
                                                else
                                                {
                                                    //next payments
                                                    var processPaymentResult = new ProcessPaymentResult
                                                    {
                                                        NewPaymentStatus = newPaymentStatus
                                                    };
                                                    if (newPaymentStatus == PaymentStatus.Authorized)
                                                        processPaymentResult.AuthorizationTransactionId = txn_id;
                                                    else
                                                        processPaymentResult.CaptureTransactionId = txn_id;

                                                    _orderProcessingService.ProcessNextRecurringPayment(rp, processPaymentResult);
                                                }
                                            }
                                            break;
                                        case PaymentStatus.Voided:
                                            //failed payment
                                            var failedPaymentResult = new ProcessPaymentResult
                                            {
                                                Errors = new[] { $"PayPal IPN. Recurring payment is {payment_status} ." },
                                                RecurringPaymentFailed = true
                                            };
                                            _orderProcessingService.ProcessNextRecurringPayment(rp, failedPaymentResult);
                                            break;
                                    }
                                }

                                //this.OrderService.InsertOrderNote(newOrder.OrderId, sb.ToString(), DateTime.UtcNow);
                                _logger.Information("PayPal IPN. Recurring info", new NopException(sb.ToString()));
                            }
                            else
                            {
                                _logger.Error("PayPal IPN. Order is not found", new NopException(sb.ToString()));
                            }
                        }
                        break;
                    case "recurring_payment_failed":
                        if (Guid.TryParse(rp_invoice_id, out Guid orderGuid))
                        {
                            var initialOrder = _orderService.GetOrderByGuid(orderGuid);
                            if (initialOrder != null)
                            {
                                var recurringPayment = _orderService.SearchRecurringPayments(initialOrderId: initialOrder.Id).FirstOrDefault();
                                //failed payment
                                if (recurringPayment != null)
                                    _orderProcessingService.ProcessNextRecurringPayment(recurringPayment, new ProcessPaymentResult { Errors = new[] { txn_type }, RecurringPaymentFailed = true });
                            }
                        }
                        break;
                    #endregion
                    default:
                        #region Standard payment
                        {
                            values.TryGetValue("custom", out string orderNumber);
                            var orderNumberGuid = Guid.Empty;
                            try
                            {
                                orderNumberGuid = new Guid(orderNumber);
                            }
                            catch
                            {
                            }

                            var order = _orderService.GetOrderByGuid(orderNumberGuid);
                            if (order != null)
                            {

                                //order note
                                order.OrderNotes.Add(new OrderNote
                                {
                                    Note = sb.ToString(),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                                _orderService.UpdateOrder(order);

                                switch (newPaymentStatus)
                                {
                                    case PaymentStatus.Pending:
                                        {
                                        }
                                        break;
                                    case PaymentStatus.Authorized:
                                        {
                                            //validate order total
                                            if (Math.Round(mc_gross, 2).Equals(Math.Round(order.OrderTotal, 2)))
                                            {
                                                //valid
                                                if (_orderProcessingService.CanMarkOrderAsAuthorized(order))
                                                {
                                                    _orderProcessingService.MarkAsAuthorized(order);
                                                }
                                            }
                                            else
                                            {
                                                //not valid
                                                var errorStr =
                                                    $"PayPal IPN. Returned order total {mc_gross} doesn't equal order total {order.OrderTotal}. Order# {order.Id}.";
                                                //log
                                                _logger.Error(errorStr);
                                                //order note
                                                order.OrderNotes.Add(new OrderNote
                                                {
                                                    Note = errorStr,
                                                    DisplayToCustomer = false,
                                                    CreatedOnUtc = DateTime.UtcNow
                                                });
                                                _orderService.UpdateOrder(order);
                                            }
                                        }
                                        break;
                                    case PaymentStatus.Paid:
                                        {
                                            //validate order total
                                            if (Math.Round(mc_gross, 2).Equals(Math.Round(order.OrderTotal, 2)))
                                            {
                                                //valid
                                                if (_orderProcessingService.CanMarkOrderAsPaid(order))
                                                {
                                                    order.AuthorizationTransactionId = txn_id;
                                                    _orderService.UpdateOrder(order);

                                                    _orderProcessingService.MarkOrderAsPaid(order);
                                                }
                                            }
                                            else
                                            {
                                                //not valid
                                                var errorStr =
                                                    $"PayPal IPN. Returned order total {mc_gross} doesn't equal order total {order.OrderTotal}. Order# {order.Id}.";
                                                //log
                                                _logger.Error(errorStr);
                                                //order note
                                                order.OrderNotes.Add(new OrderNote
                                                {
                                                    Note = errorStr,
                                                    DisplayToCustomer = false,
                                                    CreatedOnUtc = DateTime.UtcNow
                                                });
                                                _orderService.UpdateOrder(order);
                                            }
                                        }
                                        break;
                                    case PaymentStatus.Refunded:
                                        {
                                            var totalToRefund = Math.Abs(mc_gross);
                                            if (totalToRefund > 0 && Math.Round(totalToRefund, 2).Equals(Math.Round(order.OrderTotal, 2)))
                                            {
                                                //refund
                                                if (_orderProcessingService.CanRefundOffline(order))
                                                {
                                                    _orderProcessingService.RefundOffline(order);
                                                }
                                            }
                                            else
                                            {
                                                //partial refund
                                                if (_orderProcessingService.CanPartiallyRefundOffline(order, totalToRefund))
                                                {
                                                    _orderProcessingService.PartiallyRefundOffline(order, totalToRefund);
                                                }
                                            }
                                        }
                                        break;
                                    case PaymentStatus.Voided:
                                        {
                                            if (_orderProcessingService.CanVoidOffline(order))
                                            {
                                                _orderProcessingService.VoidOffline(order);
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                            else
                            {
                                _logger.Error("PayPal IPN. Order is not found", new NopException(sb.ToString()));
                            }
                        }
                        #endregion
                        break;
                }
            }
            else
            {
                _logger.Error("PayPal IPN failed.", new NopException(strRequest));
            }
*/
            //nothing should be rendered to visitor
            return Content("");
        }

        public override IList<string> ValidatePaymentForm(FormCollection form)
        {   //try to get errors
            var errors = form["Errors"];
            if (!string.IsNullOrEmpty(errors))
                return errors.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return new List<string>();
        }

        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentRequest = new ProcessPaymentRequest();

            //pass custom values to payment processor
            //    if (form.TryGetValue("ipag-token", out StringValues ipagToken) && !StringValues.IsNullOrEmpty(ipagToken))
            //        paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Ipag.Fields.IpagToken.Key"), ipagToken.ToString());

            //    if (form.TryGetValue("TokenIpag", out StringValues TokenIpag) && !StringValues.IsNullOrEmpty(TokenIpag) && !TokenIpag.Equals("0"))
            //        paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Square.Fields.TokenIpag.Key"), TokenIpag.ToString());

            //    if (form.TryGetValue("SaveCard", out StringValues saveCardValue) && !StringValues.IsNullOrEmpty(saveCardValue) && bool.TryParse(saveCardValue[0], out bool saveCard) && saveCard)
            //        paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Square.Fields.SaveCard.Key"), saveCard);

            //    if (form.TryGetValue("MethodIpag", out StringValues method) && !StringValues.IsNullOrEmpty(method) && !method.Equals("0"))
            //        paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Ipag.Fields.MethodIpag.Key"), method.ToString());

                        //pass custom values to payment processor
            var ipag_token = form["ipag-token"];
            if (!string.IsNullOrEmpty(ipag_token))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Ipag.Fields.IpagToken.Key"), ipag_token);

            var TokenIpag = form["TokenIpag"];
            if (!string.IsNullOrEmpty(TokenIpag))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Square.Fields.TokenIpag.Key"), TokenIpag);

            var saveCardValue = form["SaveCard"];
            if (!string.IsNullOrEmpty(saveCardValue) && bool.TryParse(saveCardValue.Split(',').FirstOrDefault(), out bool saveCard) && saveCard)
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Square.Fields.SaveCard.Key"), saveCard);

            var methodIpag = form["ipag-method"];
            if (!string.IsNullOrEmpty(methodIpag))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.Ipag.Fields.MethodIpag.Key"), methodIpag);

            return paymentRequest;
        }

        public string getEndpoint()
        {
            if (_ipagPaymentSettings.UseSandbox)
            {
                return ENDPOINT_SANDBOX;
            }
            return ENDPOINT_PRODUCTION;
        }
        [HttpGet, ActionName("Access")]

        public string Access(string ipagid, string ipagkey)
        {
            HttpClient client = new HttpClient();
           
            client.BaseAddress = new Uri(getEndpoint());
            client.DefaultRequestHeaders.Accept.Clear();
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(ipagid + ":" + ipagkey);
            var authToken = Convert.ToBase64String(plainTextBytes);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("User-Agent", "iPag plugin NopCommerce");
            Token token = null;
            HttpResponseMessage response = client.GetAsync("service/sessionToken").Result;
            if (response.IsSuccessStatusCode)
            {
                token = response.Content.ReadAsAsync<Token>().Result;
                return token.token;
            }
            else
            {
                return "Erro ao obter Token de Acesso";
            }
        }
    }
}
