﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Ipag.Domain
{
    public class IpagOrder
    {
        public string id;
        public decimal total;

        public IpagOrder(string id, decimal total)
        {
            this.id = id;
            this.total = total;
        }
    }
}
