﻿using Nop.Core.Domain.Customers;
using Nop.Services.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Ipag.Domain
{
   public  class IpagRequest
    {
        public Customer customer;
        public Address address;
        public IpagOrder order;
        public ProcessPaymentRequest request;

        public IpagRequest(Customer customer, Address address, IpagOrder order, ProcessPaymentRequest request)
        {
            this.customer = customer;
            this.address = address;
            this.order = order;
            this.request = request;
        }
    }
}
