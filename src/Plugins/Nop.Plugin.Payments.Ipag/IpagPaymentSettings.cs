﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace Nop.Plugin.Payments.Ipag
{
    public class IpagPaymentSettings : ISettings
    {
        static HttpClient client = new HttpClient();
        public IpagPaymentSettings()
        {
        }

        public IpagPaymentSettings(bool useSandbox, string aPIKey, string loginIpag, List<string> acceptedCards)
        {
            UseSandbox = useSandbox;
            APIKey = aPIKey;
            LoginIpag = loginIpag;
            AcceptedCards = acceptedCards;
        }

        public bool UseSandbox { get; set; }
        public String APIKey { get; set; }
        public String LoginIpag { get; set; }
        public List<string> AcceptedCards { get; set; }
    }

}
