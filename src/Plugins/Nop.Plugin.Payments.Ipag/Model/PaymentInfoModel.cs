﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.Ipag.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        #region Ctor

        public PaymentInfoModel()
        {
            StoredCards = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public bool IsGuest { get; set; }

        public string TokenIpag { get; set; }

        public string MethodIpag { get; set; }

        public string Errors { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Ipag.Fields.SaveCard")]
        public bool SaveCard { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Ipag.Fields.StoredCard")]
        public string StoredCardId { get; set; }
        public IList<SelectListItem> StoredCards { get; set; }

        #endregion
    }
}