﻿using Nop.Web.Framework.Mvc.Routes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nop.Plugin.Payments.Ipag
{
    public partial class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get { return 0; }
        }
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Payments.Ipag.Access",
                 "Plugins/Ipag/Access",
                 new { controller = "PaymentIpag", action = "Access" },
                 new[] { "Nop.Plugin.Payments.Ipag.Controllers" }
            );
        }
    }
}
